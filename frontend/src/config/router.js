import Vue from 'vue'
import VueRouter from 'vue-router'

import JHome from "../components/home/JHome";
import JAdminPages from "../components/admin/JAdminPages";
import JArticlesByCategory from "../components/article/JArticlesByCategory";
import JArticleById from "../components/article/JArticleById";
import JAuth from "../components/auth/JAuth";

import {userKey} from "../global";

Vue.use(VueRouter)

const routes = [{
    name: 'home',
    path: '/',
    component: JHome
}, {
    name: 'adminPages',
    path: '/admin',
    component: JAdminPages,
    meta: {requiresAdmin: true}
}, {
    name: 'articlesByCategory',
    path: '/categories/:id/articles',
    component: JArticlesByCategory
}, {
    name: 'articleById',
    path: '/articles/:id',
    component: JArticleById
}, {
    name: 'auth',
    path: '/auth',
    component: JAuth,
    meta: { requiresAuth: true }
}
]

const router = new VueRouter({
    mode: 'history',
    routes
})

router.beforeEach((to, from, next) => {
    const json = localStorage.getItem(userKey)

    if (to.matched.some(record => record.meta.requiresAdmin)) {
        const user = JSON.parse(json)
        user && user.admin ? next() : next({path: '/'})
    } else {
        next()
    }

    if (to.matched.some(record => record.meta.requiresAuth)) {
        const user = JSON.parse(json)
        !user ? next() : next({path: '/'})
    } else {
        next() // make sure to always call next()!
    }
})

export default router